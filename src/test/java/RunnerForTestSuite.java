
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import org.junit.internal.TextListener;
import org.junit.runner.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RunnerForTestSuite{
    @Test
    public  void  run() throws IOException, ClassNotFoundException {
        ObjectMapper mapper = new XmlMapper();

        POJO.Suite suite  = mapper.readValue(new File("src/main/resources/XmlRunner.xml"),POJO.Suite.class);

        List<String> testClasses = XmlWorker.getTestClasses(suite);
        List<MethodsPairs> testMethods = XmlWorker.getTestMethods(suite);

        List<Request> requests = new ArrayList<Request>();

        for(String clazz:testClasses){
            requests.add(Request.aClass(Class.forName(clazz)));
        }
        for (MethodsPairs pairs: testMethods){
            requests.add(Request.method(pairs.getClazz(),pairs.getMethods()));
        }


        JUnitCore runner = new JUnitCore();
        runner.addListener(new TextListener((System.out)));

        for(Request req: requests){
            Result result = runner.run(req);
            System.out.println("Failure tests: " + result.getFailures().size());
        }

    }

}
