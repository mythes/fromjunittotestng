import POJO.*;

import java.util.ArrayList;
import java.util.List;

public class XmlWorker {

   public static List<String> getTestClasses(Suite suite){

      List<String> classes = new ArrayList<String>();

      for(Test test: suite.getTest()){
         for (Clazz clazz: test.getClasses().getClazz()){
            if(clazz.getMethods() == null){
               classes.add(clazz.getName());
            }
         }
      }

      for(String name: classes){
          System.out.println("---------------------");
          System.out.println("Class for run: " + name);
          System.out.println("---------------------");
      }

      return classes;
   }

   public static List<MethodsPairs> getTestMethods(Suite suite) throws ClassNotFoundException {

      List<MethodsPairs> actualMethods = new ArrayList<MethodsPairs>();

      for(Test test: suite.getTest()){
         for (Clazz clazz: test.getClasses().getClazz()){
            if(clazz.getMethods()!= null){
               for(Methods methodz: clazz.getMethods()){
                  for(Include include: methodz.getInclude()){
                      actualMethods.add(new MethodsPairs(Class.forName(clazz.getName()),include.getName()));
                  }
               }
            }
         }
      }

      for(MethodsPairs m: actualMethods){
          System.out.println("---------------------");
          System.out.println("From class : " + m.getClazz());
          System.out.println("methods for run : " + m.getMethods());
          System.out.println("---------------------");
      }

      return actualMethods;
   }



}
