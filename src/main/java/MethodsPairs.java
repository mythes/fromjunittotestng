
public class MethodsPairs {

    private String methods;
    private Class clazz;

    public MethodsPairs(Class clazz, String methods){
        this.clazz = clazz;
        this.methods = methods;
    }

    public Class getClazz() {
        return clazz;
    }


    public String getMethods() {
        return methods;
    }


}
