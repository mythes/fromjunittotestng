package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Clazz {

    @JacksonXmlProperty(localName = "name", isAttribute = true)
    String name;

    @JacksonXmlProperty(localName = "methods")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Methods> methods;

    public String getName() {
        return name;
    }
    public List<Methods> getMethods() {
        return methods;
    }

}
