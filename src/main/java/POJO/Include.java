package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Include {
    @JacksonXmlProperty(localName = "name",isAttribute = true)
    @JacksonXmlElementWrapper(useWrapping = false)
    private String name;

    public String getName() {
        return name;
    }


}
