package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Methods {
    @JacksonXmlProperty(localName = "include")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Include> include;

    public List<Include> getInclude() {
        return include;
    }
}
