package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Test {

    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    @JacksonXmlProperty(localName = "classes")
    private Classes classes;

    public String getName() {
        return name;
    }

    public Classes getClasses() {
        return classes;
    }


}
